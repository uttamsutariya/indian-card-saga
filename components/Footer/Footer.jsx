import Image from "next/image";
import styles from "./Footer.module.scss";
import Link from "next/link";

const Footer = () => {
    return (
        <div className={styles.footerContainer}>
            <div className={styles.footerRow}>
                <div className={styles.column}>
                    <Link href="/">
                        <Image src="/assets/epic-teen-patti.jpeg" alt="Teen Patti Epic" width={150} height={150} />
                    </Link>
                </div>
                <div className={styles.column}>
                    <h3>About Us</h3>
                    <div>
                        <Link href="/">Home</Link>
                        <Link href="/about-us">About Us</Link>
                        <Link href="/how-to-play">How to play</Link>
                        <Link href="/contact-us">Contact Us</Link>
                    </div>
                </div>
                <div className={styles.column}>
                    <h3>Important Links</h3>
                    <div>
                        <Link href="/">Responsible Gaming</Link>
                        <Link href="/">Fair Play</Link>
                        <Link href="/">Terms & Conditions</Link>
                        <Link href="/">Privacy Policy</Link>
                    </div>
                </div>
                <div className={styles.column}>
                    <h3>Download</h3>
                    <div>
                        <Link href="/">Responsible Gaming</Link>
                    </div>
                </div>
            </div>
            <div className={styles.rights}>
                <p>All Rights Reserved by Teen Patti Epic</p>
                <p>Copyright ©2024</p>
            </div>
        </div>
    );
};

export default Footer;
