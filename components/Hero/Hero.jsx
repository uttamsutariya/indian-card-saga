import Link from "next/link";
import Button from "../Button/Button";
import styles from "./Hero.module.scss";

const Hero = ({ isHomePage = false, label }) => {
    return (
        <div className={styles.hero}>
            {/* {isHomePage && (
                <div>
                    <Button label={"Win Real Money"} />
                </div>
            )} */}
            <div className={styles.heroTextGroup}>
                {isHomePage ? (
                    <p>
                        Win Real Money Rewards With <br /> <span className={styles.heroTextOrange}>Teen Patti Epic</span>{" "}
                    </p>
                ) : (
                    <p>{label}</p>
                )}
            </div>
            <div className={styles.heroButtonGroup}>
                <Link href="https://refer9.com/epic/re0zbx?f=w&p=wa&l=en&tp=epic1">
                    <Button label={"Download Now"} />
                </Link>
                <Link href="/how-to-play">
                    <Button label={"How To Play"} isOutlined />
                </Link>
            </div>
        </div>
    );
};

export default Hero;
