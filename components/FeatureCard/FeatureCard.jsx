import Image from "next/image";
import styles from "./FeatureCard.module.scss";
import ArrowDownIcon from "../../public/assets/icon.svg";

const FeatureCard = ({ heading, boldText, description, showSteps = false }) => {
    return (
        <div className={styles.cardContainer}>
            <div>
                <Image
                    style={{
                        alignSelf: "flex-start",
                    }}
                    src={ArrowDownIcon}
                    width={30}
                    height={30}
                />
                {showSteps && (
                    <h1>
                        <span>01</span>
                    </h1>
                )}
            </div>
            <div>
                <div>
                    <h3>{heading}</h3>
                </div>
                <div>
                    <p>
                        <span className={styles.boldText}>{boldText}</span> <span>{description}</span>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default FeatureCard;
