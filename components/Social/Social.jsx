import Image from "next/image";
import styles from "./Social.module.scss";

const Social = () => {
    return (
        <div className={styles.socialContainer}>
            <div>
                <h1>Social Links</h1>
                <div className={styles.imageContainer}>
                    <a href="https://www.facebook.com/people/Indian-Card-Saga/61555807562425/" target="_blank">
                        <Image src="/assets/facebook.png" alt="Facebook" width={70} height={70} />
                    </a>
                    <a href="https://www.instagram.com/indiancardsaga/" target="_blank">
                        <Image src="/assets/instagram.png" alt="Instagram" width={70} height={70} />
                    </a>
                    <a href="https://www.youtube.com/channel/UCyfsDnlcf44h7VIzu1kMEJA" target="_blank">
                        <Image src="/assets/youtube.png" alt="Youtube" width={70} height={70} />
                    </a>
                </div>
            </div>
        </div>
    );
};

export default Social;
