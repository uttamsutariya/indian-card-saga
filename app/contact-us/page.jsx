import Footer from "@/components/Footer/Footer";
import Form from "@/components/Form/Form";
import Hero from "@/components/Hero/Hero";
import PlayToEarn from "@/components/PlayToEarn/PlayToEarn";
import Social from "@/components/Social/Social";
import styles from "@/styles/About.module.scss";
import Image from "next/image";

const ContactUs = () => {
    return (
        <div className={["wrapper"]}>
            <div className={"backgroundImageContainer"}>
                <img src="/assets/bg-image.png" alt="" />
            </div>
            <Hero label={"Contact Us"} />
            <div className={styles.textContainer}>
                <p>
                    If you run into any issues while using the Teen Patti Epic app, whether it’s about adding funds or making withdrawals, know that the app places a strong
                    emphasis on providing top-notch customer support. We have a professional team of experts available 24/7 to address your issues and complaints.
                </p>
                <p>
                    We offer a variety of customer services, including support options for recharges, which are at your fingertips. Customer satisfaction is our top priority, and
                    the Teen Patti Epic app is prepared to assist you promptly.
                </p>
            </div>
            <PlayToEarn
                buttonLabel={"Contact Us"}
                labelWhite={"Get In Touch With"}
                labelOrange={"Us"}
                description={
                    "Mastering Teen Patti is a skill that evolves over time, and we believe you'll reach pro status with dedicated practice. Dive into a journey filled with fun and excitement, where the thrill of the game is matched by exciting bonuses and rewards. If you seek a world-class Teen Patti experience, connect with our experts now and elevate your gaming adventure. The path to excellence begins with each card dealt! Download the app now!"
                }
                rightChildComponent={<Form />}
            />
            <Social />
            <Footer />
        </div>
    );
};

export default ContactUs;
