"use client";

import styles from "../styles/Home.module.scss";
import Hero from "@/components/Hero/Hero";
import FeatureCard from "@/components/FeatureCard/FeatureCard";
import Image from "next/image";
import StepsCard from "@/components/StepsCard/StepsCard";
import Social from "@/components/Social/Social";
import Footer from "@/components/Footer/Footer";
import PlayToEarn from "@/components/PlayToEarn/PlayToEarn";
import Script from "next/script";

export default function Home() {
    return (
        <>
            <Script
                src="https://www.googletagmanager.com/gtag/js?id=G-4XL5ZWE2BF"
                onReady={() => {
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag("js", new Date());
                    gtag("config", "G-4XL5ZWE2BF");
                }}
            />
            <Script
                src="https://www.googletagmanager.com/gtm.js?id=GTM-MJ94XRNC"
                onReady={() => {
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag("js", new Date());
                    gtag("config", "GTM-MJ94XRNC");
                }}
            />

            <Script
                src="'https://connect.facebook.net/en_US/fbevents.js"
                onReady={() => {
                    !(function (f, b, e, v, n, t, s) {
                        if (f.fbq) return;
                        n = f.fbq = function () {
                            n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments);
                        };
                        if (!f._fbq) f._fbq = n;
                        n.push = n;
                        n.loaded = !0;
                        n.version = "2.0";
                        n.queue = [];
                        t = b.createElement(e);
                        t.async = !0;
                        t.src = v;
                        s = b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t, s);
                    })(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js");
                    fbq("init", "713962067388689");
                    fbq("track", "PageView");
                }}
            />

            <div className={["wrapper"]}>
                <div className={"backgroundImageContainer"}>
                    <img src="/assets/bg-image.png" alt="" />
                </div>
                <Hero isHomePage />
                <div className={styles.featuresContainer}>
                    <p className={styles.textRegular}>
                        Teen Patti Epic is a game that requires both skills and strategy. As one of the most popular online real-money gaming apps in India, it has gained the trust
                        of millions of online players who are invested in real-money card games. You can download the game on both Android and iOS devices with internet
                        connectivity of 4G and above. The Teen Patti Epic game is packed with a healthy amount of challenges, the latest and most tech-advanced features and
                        functionalities, stunning graphics and animation, and high- quality sound effects to keep users engaged for a prolonged period of time. 3 patti epic also
                        offers an exciting bonus of up to Rs 1550 for new players.
                    </p>
                    <h1>The exciting features it offers</h1>
                    <p className={styles.textRegular}>
                        Teen Patti Epic APK contains 20+ games including Teen Patti and Rummy, which are the most popular card games and are played by millions of Indians online.
                        The Teen Patti Epic is an amazing game that offers an immaculate gaming experience due to the several bonuses and advanced and complex features incorporated
                        into it. From log-in bonuses to multiple payment gateways, the app has it all.
                    </p>
                </div>
                <div className={styles.featuresCardRow}>
                    <FeatureCard
                        heading={"Exciting Deposite Offers"}
                        boldText={"Teen Patti Epic APK download"}
                        description={"provides valuable offers to users on deposits.Players can use them while depositing and receive extra rewards."}
                    />
                    <FeatureCard
                        heading={"Instant Withdrawal"}
                        boldText={"From the Teen Patti Epic apk,"}
                        description={"you can instantly withdraw your money into the bank account without needing KYC for a PAN card."}
                    />
                    <FeatureCard
                        heading={"Login Bonus"}
                        boldText={"Teen Patti Epic"}
                        description={"app provides bonuses and rewards with energy log-in. It motivates users to play the game every day."}
                    />
                    <FeatureCard
                        heading={"VIP Programs"}
                        boldText={"Download Teen Patti Epic APK for Android"}
                        description={
                            "and iOS for VIP programmes through which players can earn daily, weekly, and monthly bonuses. But first, they need to deposit Rs 100 in your wallet app."
                        }
                    />
                </div>
                <PlayToEarn
                    buttonLabel={"Win Real Money"}
                    labelWhite={"Play To Earn Real"}
                    labelOrange={"Cash Rewards"}
                    description={
                        "Teen Patti Epic latest version contains a wide collection of real cash-earning games, including rummy, slot games, poker, Andar Bahar, and more. Delve into the exciting experience of playing traditional games such as rummy, teen patti, and more fun games on your mobile device and win exciting cash rewards and bonuses. The Teen Patti Epic apk allows seamless money transactions, so you can easily deposit or withdraw your winning amount at any given point of time with top-notch security. So, what are you waiting for? Invite your family and friends to the Teen Patti Epic app and challenge them to play a round without any boundaries of distance or time."
                    }
                    rightChildComponent={<Image src="/assets/card-image.png" alt="Play to earn" width={400} height={400} />}
                />
                <div className={styles.howToPlayContainer}>
                    <div className={styles.textContainer}>
                        <h1>How should we play Teen Patti Epic?</h1>
                        <p>
                            The gameplay of Teen Patti Epic is quite simple and seamless, like other teenpatti games. However, this one offers a more engaging and entertaining
                            experience. In order to play Teen Patti Epic, you have to download and install the Teen Patti Epic mod apk from any browser (Chrome, Binge) on your
                            mobile device and register your steps to learn how to play Indian Card for real money. Here’s what you have to do: details. Follow the below
                        </p>
                    </div>
                    <div className={styles.cardsRow}>
                        <div className={styles.column}>
                            <StepsCard
                                stepNumber={"01"}
                                heading={"Choose a game mode."}
                                description={"Teen Patti Epic offers various game modes; select the mode in which you want to play."}
                            />
                            <StepsCard
                                stepNumber={"02"}
                                heading={"Enter a table and place bet"}
                                description={"Select the table with the desired stakes and numbers, and place the bets according to the table's betting limits."}
                            />
                        </div>
                        <div className={styles.column}>
                            <Image className={styles.cardImg} src="/assets/steps.png" alt="Play to earn" width={400} height={400} />
                        </div>
                        <div className={styles.column}>
                            <StepsCard
                                stepNumber={"03"}
                                heading={"Play Your Cards"}
                                description={"Teen Patti Epic deals three cards to every player. Use the card from the best possible hand to beat your opponents."}
                            />
                            <StepsCard stepNumber={"04"} heading={"Collect your winnings"} description={"If you hand beat the opponent, you’ll collect the winning amount."} />
                        </div>
                    </div>
                </div>
                <Social />
                <Footer />
            </div>
        </>
    );
}
